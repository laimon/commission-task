# Commission task

An application that handles operations provided in CSV format and calculates a commission fee based on defined rules.
## Requirements

Before run an application you must:
* Install PHP 7.2 or higher.
* Install Composer, which is used to install PHP packages

## Installation
Clone the application to download its contents
```
git clone https://laimon@bitbucket.org/laimon/commission-task.git
```

Composer helps install dependencies of project
```
composer install
```

## Usage

Run script in command-line interface
```
php script.php
```
