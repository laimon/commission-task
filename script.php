<?php

require_once __DIR__ . './vendor/autoload.php';

use CommissionTask\Reader\CsvReader;
use CommissionTask\Service\FeeCalculator;
use CommissionTask\Operation\Operation;

use CommissionTask\UserWeekRegistry;

use CommissionTask\CurrencyRateRegistry;
use CommissionTask\Service\CurrencyRate\Provider\DummyProvider;
use CommissionTask\Service\CurrencyRate\Provider\RatesProvider;
use CommissionTask\Service\CurrencyRate\Provider\ExchangeRatesProvider;

$currencyRateRegistry = new CurrencyRateRegistry();

$currencyRateRegistry->set('dummy', new DummyProvider);
$currencyRateRegistry->set('rates', new RatesProvider);
$currencyRateRegistry->set('exchangerates', new ExchangeRatesProvider);

$userWeekRegistry = new UserWeekRegistry();

$currencyRate = $currencyRateRegistry->get('exchangerates');

$csvFile = 'input.csv';
if (isset($argv[1]) && file_exists($argv[1])) {
    $csvFile = $argv[1];
}
$file = new SplFileObject($csvFile);

$reader = new CsvReader($file);
$file = null;
$reader->setColumnHeaders(['date', 'user_id', 'user_type', 'type', 'amount', 'currency']);

$operations = [];
$userWeek = [];
foreach($reader as $row) {
    $operation = new Operation(
        $row['date'],
        $row['user_id'],
        $row['user_type'],
        $row['type'],
        $row['amount'],
        $row['currency']
    );
    
    if ($operation->getCurrency() !== 'EUR') {
        $rateResult = $currencyRate->setDate($operation->getDate())
            ->setTarget(['USD', 'JPY'])
            ->get();
        $operation->setRate($rateResult->getRate($operation->getCurrency()));
    }
    if($operation->getUserType() === 'private' && $operation->getType() === 'withdraw') {
        $eurAmount = $operation->getAmount()/$operation->getRate();
        $userWeekRegistry->set(
            $operation->getUserId(),
            $operation->getWeekYear(),
            $operation->getWeek(),
            $eurAmount,$operation->getType()
        );
        $userWeek = $userWeekRegistry->get(
            $operation->getUserId(),
            $operation->getWeekYear(),
            $operation->getWeek()
        );
        $operation->setWeekAmount($userWeek['amount']);
        $operation->setWeekOperationNumber($userWeek['operationNumber']);
    }
    $operations[] = $operation;
}

$feeCalculator = new FeeCalculator($operations);
$feeCalculator->calculate();

foreach ($operations as $operation) {
    echo $operation . PHP_EOL;
}
