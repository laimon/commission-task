<?php

declare(strict_types=1);

namespace CommissionTask;

use CommissionTask\Fee\Fee;

class FeeRegistry
{
    private static array $fees;

    public static function set(string $name, Fee $fee): void
    {
        self::$fees[$name] = $fee;
    }

    public static function get(string $name): Fee
    {
        return self::$fees[$name];
    }
}
