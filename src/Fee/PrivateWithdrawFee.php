<?php

declare(strict_types=1);

namespace CommissionTask\Fee;

class PrivateWithdrawFee extends Fee
{
    public function calculate(): float
    {
        $amount = $this->operation->getAmount();
        $weekAmount = $this->operation->getWeekAmount();
        $rate = $this->operation->getRate();
        $currencyDecimal = $this->operation::CURR_DEC[$this->operation->getCurrency()];

        if (
            $this->operation->getWeekOperationNumber() > 3 ||
            $weekAmount >= 0 && $weekAmount * $rate > $amount
        ) {
            return $this->round($amount * 0.003, $currencyDecimal);
        } elseif ($weekAmount >= 0 && $weekAmount * $rate < $amount) {
            return $this->round($weekAmount * $rate * 0.003, $currencyDecimal);
        } else {
            return 0;
        }
    }
}
