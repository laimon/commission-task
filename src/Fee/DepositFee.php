<?php

declare(strict_types=1);

namespace CommissionTask\Fee;

class DepositFee extends Fee
{
    public function calculate(): float
    {
        return $this->round(
            $this->operation->getAmount() * 0.0003,
            $this->operation::CURR_DEC[$this->operation->getCurrency()]
        );
    }
}
