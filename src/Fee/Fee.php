<?php

declare(strict_types=1);

namespace CommissionTask\Fee;

use CommissionTask\Operation\Operation;

abstract class Fee
{
    public Operation $operation;

    public function setOperation(Operation $operation): self
    {
        $this->operation = $operation;

        return $this;
    }

    public function round(float $val, int $precision = 2): float
    {
        $fig = pow(10, $precision);

        return ceil($val * $fig) / $fig;
    }

    abstract public function calculate(): float;
}
