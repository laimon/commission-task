<?php

declare(strict_types=1);

namespace CommissionTask\Fee;

class BusinessWithdrawFee extends Fee
{
    public function calculate(): float
    {
        return $this->round(
            $this->operation->getAmount() * 0.005,
            $this->operation::CURR_DEC[$this->operation->getCurrency()]
        );
    }
}
