<?php

declare(strict_types=1);

namespace CommissionTask;

class UserWeekRegistry
{
    public array $registry;

    public function __construct()
    {
        $this->registry = [];
    }

    public function set(int $userId, int $year, int $week, float $amount): void
    {
        if (!array_key_exists($userId, $this->registry)) {
            $this->registry[$userId] = [];
        }

        if (!array_key_exists($year, $this->registry[$userId])) {
            $this->registry[$userId][$year] = [];
        }

        if (!array_key_exists($week, $this->registry[$userId][$year])) {
            $this->registry[$userId][$year][$week] = [
                'amount' => -1000,
                'operationNumber' => 0,
            ];
        }

        $this->registry[$userId][$year][$week]['amount'] += $amount;
        ++$this->registry[$userId][$year][$week]['operationNumber'];
    }

    public function get(int $userId, int $year, int $week): array
    {
        return $this->registry[$userId][$year][$week];
    }
}
