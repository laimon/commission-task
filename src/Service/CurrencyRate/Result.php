<?php

declare(strict_types=1);

namespace CommissionTask\Service\CurrencyRate;

use DateTime;

class Result
{
    protected string $base;

    protected DateTime $date;

    protected array $rates;

    public function __construct(string $base, DateTime $date, array $rates)
    {
        $this->base = $base;
        $this->date = $date;
        $this->rates = $rates;
    }

    public function getBase(): string
    {
        return $this->base;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function getRates(): array
    {
        return $this->rates;
    }

    public function getRate(string $code): ?float
    {
        if ($code === $this->getBase()) {
            return 1.0;
        }

        if (isset($this->rates[$code])) {
            return $this->rates[$code];
        }

        return null;
    }
}
