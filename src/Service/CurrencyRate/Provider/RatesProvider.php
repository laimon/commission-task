<?php

declare(strict_types=1);

namespace CommissionTask\Service\CurrencyRate\Provider;

use CommissionTask\Service\CurrencyRate\Provider;
use CommissionTask\Service\CurrencyRate\Result;
use DateTime;

class RatesProvider extends Provider
{
    private array $currencies = ['EUR', 'JPY', 'USD'];

    private string $url = 'https://api.ratesapi.io/api';

    public function getHistorical(DateTime $date, string $base = 'EUR', array $targets = []): Result
    {
        $response = $this->query($date->format('Y-m-d'), $base, $targets);

        return new Result($base, $date, $response['rates']);
    }

    public function query(string $date, string $base, array $targets): array
    {
        $url = $this->url.'/'.$date;
        $query = [];

        $query[] = 'base='.$base;

        if (!empty($targets)) {
            $query[] = 'symbols='.implode(',', $targets);
        }

        if (!empty($query)) {
            $url .= '?'.implode('&', $query);
        }

        //TODO: request, response and caching classes
        $cachetime = 604800;
        $where = 'cache';
        if (!is_dir($where)) {
            @mkdir($where, 0755, true);
        }

        $hash = md5($url);
        $file = "$where/$hash.cache";

        $mtime = 0;
        if (file_exists($file)) {
            $mtime = filemtime($file);
        }
        $filetimemod = $mtime + $cachetime;

        if ($filetimemod < time()) {
            $ch = curl_init();

            curl_setopt_array($ch, [
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false,
            ]);

            $json = curl_exec($ch);
            curl_close($ch);
            if ($json !== false) {
                file_put_contents($file, $json);
                chmod($file, 0755);
            }
        } else {
            $json = file_get_contents($file);
        }

        $conversionResult = [];
        if (!is_bool($json)) {
            $conversionResult = json_decode($json, true);
        }

        return $conversionResult;
    }
}
