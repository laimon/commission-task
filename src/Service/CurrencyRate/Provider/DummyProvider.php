<?php

declare(strict_types=1);

namespace CommissionTask\Service\CurrencyRate\Provider;

use CommissionTask\Service\CurrencyRate\Provider;
use CommissionTask\Service\CurrencyRate\Result;
use DateTime;
use InvalidArgumentException;

class DummyProvider extends Provider
{
    private array $currencies = ['EUR', 'JPY', 'USD'];

    public function getHistorical(DateTime $date, string $base = 'EUR', array $targets = []): Result
    {
        $key = array_search($base, $this->currencies, true);
        if ($key === false) {
            throw new InvalidArgumentException("Invalid base currency specified: $base");
        }

        if (empty($targets)) {
            $currencies = $this->currencies;
            unset($currencies[$key]);
        } else {
            $currencies = $targets;
        }

        // generate dummy results
        $res = [];
        foreach ($currencies as $currency) {
            if (!in_array($currency, $this->currencies, true)) {
                throw new InvalidArgumentException("Invalid target currency specified: $currency");
            }

            $res[$currency] = mt_rand(10000, 150000) / 100000;
        }

        // return the results
        return new Result($base, $date, $res);
    }
}
