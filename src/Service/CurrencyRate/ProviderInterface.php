<?php

declare(strict_types=1);

namespace CommissionTask\Service\CurrencyRate;

use DateTime;

interface ProviderInterface
{
    public function getHistorical(DateTime $date, string $base = 'EUR', array $targets = []): Result;

    public function setBase(string $currency): self;

    public function setTarget(array $currencies): self;

    public function setDate(DateTime $date): self;

    public function get(): Result;
}
