<?php

declare(strict_types=1);

namespace CommissionTask\Service\CurrencyRate;

use DateTime;
use UnexpectedValueException;

abstract class Provider implements ProviderInterface
{
    private string $base = 'EUR';
    private array $targets = [];
    private DateTime $date;

    public function setBase(string $currency): self
    {
        $this->base = $currency;

        return $this;
    }

    public function getBase(): string
    {
        return $this->base;
    }

    public function setTarget(array $currencies): self
    {
        $this->targets = $currencies;

        return $this;
    }

    public function getTarget(): array
    {
        return $this->targets;
    }

    public function setDate(DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function get(): Result
    {
        $result = $this->getHistorical($this->date, $this->base, $this->targets);

        if (!$result instanceof Result) {
            throw new UnexpectedValueException('Invalid result type');
        }

        return $result;
    }
}
