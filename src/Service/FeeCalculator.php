<?php

declare(strict_types=1);

namespace CommissionTask\Service;

use CommissionTask\Fee\BusinessWithdrawFee;
use CommissionTask\Fee\DepositFee;
use CommissionTask\Fee\PrivateWithdrawFee;
use CommissionTask\FeeRegistry;
use CommissionTask\Operation\Operation;

class FeeCalculator
{
    /**
     * @var array<Operation>
     */
    private array $operations;

    /**
     * @param array<Operation> $operations
     */
    public function __construct(array $operations)
    {
        $this->operations = $operations;

        FeeRegistry::set('businessdeposit', new DepositFee());
        FeeRegistry::set('privatedeposit', new DepositFee());
        FeeRegistry::set('businesswithdraw', new BusinessWithdrawFee());
        FeeRegistry::set('privatewithdraw', new PrivateWithdrawFee());
    }

    public function calculate(): void
    {
        foreach ($this->operations as $operation) {
            $fee = FeeRegistry::get($operation->getUserType().$operation->getType());
            $fee->setOperation($operation);
            $feeAmount = $fee->calculate();
            $operation->setFeeAmount($feeAmount);
        }
    }
}
