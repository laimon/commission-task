<?php

declare(strict_types=1);

namespace CommissionTask\Reader;

use SplFileObject;

class CsvReader implements ReaderInterface
{
    public SplFileObject $file;

    protected array $columnHeaders;

    protected int $headersCount;

    protected ?int $count = null;

    protected string $delimeter;

    protected string $enclosure;

    protected string $escape;

    public function __construct(
        SplFileObject $file,
        string $delimiter = ',',
        string $enclosure = '"',
        string $escape = '\\'
    ) {
        ini_set('auto_detect_line_endings', 'true');

        $this->file = $file;
        $this->file->setFlags(
            SplFileObject::READ_CSV |
            SplFileObject::SKIP_EMPTY |
            SplFileObject::READ_AHEAD |
            SplFileObject::DROP_NEW_LINE
        );
        $this->file->setCsvControl($delimiter, $enclosure, $escape);
    }

    public function rewind(): void
    {
        $this->file->rewind();
    }

    public function current(): ?array
    {
        do {
            $line = $this->file->current();
            if (!is_array($line)) {
                $line = [];
            }

            $array = array_combine(array_keys($this->columnHeaders), $line);
            if (!$array) {
                $array = null;
            }

            return $array;
        } while ($this->valid());
    }

    public function setColumnHeaders(array $columnHeaders): void
    {
        $this->columnHeaders = array_count_values($columnHeaders);
        $this->headersCount = count($columnHeaders);
    }

    public function count(): ?int
    {
        if ($this->count === null) {
            $position = $this->key();
            $this->count = iterator_count($this);
            $this->seek($position);
        }

        return $this->count;
    }

    public function next(): void
    {
        $this->file->next();
    }

    public function valid(): bool
    {
        return $this->file->valid();
    }

    public function key(): int
    {
        return $this->file->key();
    }

    public function seek(int $pointer): void
    {
        $this->file->seek($pointer);
    }
}
