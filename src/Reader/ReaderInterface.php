<?php

declare(strict_types=1);

namespace CommissionTask\Reader;

interface ReaderInterface extends \Iterator, \Countable
{
}
