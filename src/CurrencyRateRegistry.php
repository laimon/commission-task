<?php

declare(strict_types=1);

namespace CommissionTask;

use CommissionTask\Service\CurrencyRate\Provider;
use InvalidArgumentException;

class CurrencyRateRegistry
{
    private array $drivers;

    public function set(string $key, Provider $value): self
    {
        $this->drivers[$key] = $value;

        return $this;
    }

    public function get(string $key): Provider
    {
        if (!isset($this->drivers[$key])) {
            throw new InvalidArgumentException('Invalid key given');
        }

        return $this->drivers[$key];
    }
}
