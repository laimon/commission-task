<?php

declare(strict_types=1);

namespace CommissionTask\Operation;

use DateTime;

class Operation
{
    public const CURR_DEC = [
        'EUR' => 2,
        'USD' => 2,
        'JPY' => 0,
    ];

    private DateTime $date;

    private int $userId;

    private string $userType;

    private string $type;

    private float $amount;

    private string $currency;

    private int $week;

    private int $weekYear;

    private float $feeAmount = 0.00;

    private float $weekAmount = -1000;

    private int $weekOperationNumber = 0;

    private float $rate = 1;

    public function __construct(
        string $date,
        int $userId,
        string $userType,
        string $type,
        float $amount,
        string $currency
    ) {
        $this->setDate($date);
        $this->setUserId($userId);
        $this->setUserType($userType);
        $this->setType($type);
        $this->setAmount($amount);
        $this->setCurrency($currency);
        $this->setWeek((int) $this->getDate()->format('W'));
        $this->setWeekYear($this->getDate(), $this->getWeek());
    }

    public function setDate(string $date): self
    {
        $this->date = new DateTime($date);

        return $this;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setUserId(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUserType(string $userType): self
    {
        $this->userType = $userType;

        return $this;
    }

    public function getUserType(): string
    {
        return $this->userType;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function setWeek(int $week): self
    {
        $this->week = $week;

        return $this;
    }

    public function getWeek(): int
    {
        return $this->week;
    }

    public function setWeekAmount(float $weekAmount): self
    {
        $this->weekAmount = $weekAmount;

        return $this;
    }

    public function getWeekAmount(): float
    {
        return $this->weekAmount;
    }

    public function setWeekOperationNumber(int $weekOperationNumber): self
    {
        $this->weekOperationNumber = $weekOperationNumber;

        return $this;
    }

    public function getWeekOperationNumber(): int
    {
        return $this->weekOperationNumber;
    }

    public function setWeekYear(DateTime $date, int $week): self
    {
        $month = (int) $this->date->format('n');
        $year = (int) $this->date->format('Y');
        if ($week >= 52 && $month === 1) {
            --$year;
        } elseif ($week === 1 && $month === 12) {
            ++$year;
        }
        $this->weekYear = $year;

        return $this;
    }

    public function getWeekYear(): int
    {
        return $this->weekYear;
    }

    public function setRate(float $rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    public function getRate(): float
    {
        return $this->rate;
    }

    public function __toString(): string
    {
        return $this->toStringFeeAmount();
    }

    public function setFeeAmount(float $feeAmount): self
    {
        $this->feeAmount = $feeAmount;

        return $this;
    }

    public function getFeeAmount(): float
    {
        return $this->feeAmount;
    }

    public function toStringFeeAmount(): string
    {
        return number_format($this->getFeeAmount(), self::CURR_DEC[$this->getCurrency()], '.', '');
    }
}
