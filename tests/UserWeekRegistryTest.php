<?php

declare(strict_types=1);

namespace Tests;

use CommissionTask\UserWeekRegistry;
use PHPUnit\Framework\TestCase;

class UserWeekRegistryTest extends TestCase
{
    public function testSet(): void
    {
        $userId = 1;
        $year = 2016;
        $week = 1;
        $amount = 1000.00;
        $weekAmount = 0;

        $userWeekRegistry = new UserWeekRegistry();
        $userWeekRegistry->set($userId, $year, $week, $amount);
        $userWeek = $userWeekRegistry->get($userId, $year, $week);
        $this->assertEquals($weekAmount, $userWeek['amount']);
        $this->assertEquals(1, $userWeek['operationNumber']);
    }
}
