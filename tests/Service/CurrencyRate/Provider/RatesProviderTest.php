<?php

declare(strict_types=1);

namespace Tests\Service\CurrencyRate\Provider;

use CommissionTask\Service\CurrencyRate\Provider\RatesProvider;
use CommissionTask\Service\CurrencyRate\Result;
use DateTime;
use PHPUnit\Framework\TestCase;

class RatesProviderTest extends TestCase
{
    private array $responseQuery = [
        'base' => 'EUR',
        'date' => '2016-01-07',
        'rates' => [
            'USD' => 1.092459,
            'JPY' => 128.7414
        ]
    ];

    public function testQuery(): void
    {
        $sub = $this->createMock(RatesProvider::class);
        $sub->expects($this->any())
            ->method('query')
            ->will($this->returnValue($this->responseQuery))
        ;

        $date = '2016-01-07';
        $base = 'EUR';
        $targets = ['USD', 'JPY'];

        $query = $sub->query($date, $base, $targets);

        $this->assertEquals($date, $query['date']);
        $this->assertEquals($base, $query['base']);
        foreach ($targets as $target) {
            $this->assertArrayHasKey($target, $query['rates']);
        }
    }

    public function testGetHistorical(): void
    {
        $date = new DateTime('2016-01-07');
        $base = 'EUR';
        $targets = ['USD', 'JPY'];

        $response = $this->getResponseHistorical()->getHistorical($date, $base, $targets);
        $this->assertInstanceOf(DateTime::class, $response->getDate());
        $this->assertSame($base, $response->getBase());
        foreach (['USD', 'JPY'] as $rate) {
            $this->assertArrayHasKey($rate, $response->getRates());
        }
    }

    private function getResponseHistorical(): RatesProvider
    {
        $sub = $this->createMock(RatesProvider::class);
        $sub->expects($this->any())
            ->method('getHistorical')
            ->will($this->returnValue(
                new Result('EUR', new DateTime('2016-01-07'), ['USD' => 1.092459, 'JPY' => 128.7414])
            ))
        ;
        return $sub;
    }
}
