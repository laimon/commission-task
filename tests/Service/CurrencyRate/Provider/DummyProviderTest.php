<?php

declare(strict_types=1);

namespace Tests\Service\CurrencyRate\Provider;

use CommissionTask\Service\CurrencyRate\Provider;
use CommissionTask\Service\CurrencyRate\Provider\DummyProvider;
use CommissionTask\Service\CurrencyRate\Result;
use DateTime;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class DummyProviderTest extends TestCase
{
    private Provider $driver;

    public function setUp(): void
    {
        $this->driver = new DummyProvider();
    }

    public function tearDown(): void
    {
        unset($this->driver);
    }

    public function testGetHistorical(): void
    {
        $result = $this->driver->getHistorical(new DateTime(), 'EUR', ['USD', 'JPY']);

        $this->assertInstanceOf(Result::class, $result);
    }

    public function testGetHistoricalEmptyTargets(): void
    {
        $result = $this->driver->getHistorical(new DateTime(), 'EUR');

        $this->assertInstanceOf(Result::class, $result);
    }

    public function testInvalidBaseCurrencySpecified(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid base currency specified: GBP');
        $result = $this->driver->getHistorical(new DateTime(), 'GBP', ['USD', 'JPY']);
    }

    public function testInvalidTargetCurrencySpecified(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid target currency specified: GBP');
        $result = $this->driver->getHistorical(new DateTime(), 'EUR', ['USD', 'GBP']);
    }

    public function testGet(): void
    {
        $this->driver->setDate(new DateTime());
        $result = $this->driver->get();
        $this->assertInstanceOf(Result::class, $result);
    }
}
