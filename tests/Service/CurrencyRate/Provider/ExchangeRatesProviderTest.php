<?php

declare(strict_types=1);

namespace Tests\Service\CurrencyRate\Provider;

use CommissionTask\Service\CurrencyRate\Provider\ExchangeRatesProvider;
use CommissionTask\Service\CurrencyRate\Result;
use DateTime;
use PHPUnit\Framework\TestCase;

class ExchangeRatesProviderTest extends TestCase
{
    private array $responseQuery = [
        'success' => true,
        'timestamp' => 1452211199,
        'historical' => true,
        'base' => 'EUR',
        'date' => '2016-01-07',
        'rates' => [
            'USD' => 1.092459,
            'JPY' => 128.7414
        ]
    ];

    public function testSetBase(): void
    {
        $base = 'USD';
        $provider = new ExchangeRatesProvider();
        $provider->setBase($base);
        $this->assertSame($base, $provider->getBase());
    }

    public function testSetTarget(): void
    {
        $currencies = ['USD', 'JPY'];
        $provider = new ExchangeRatesProvider();
        $provider->setTarget($currencies);
        $this->assertSame($currencies, $provider->getTarget());
    }

    public function testSetDate(): void
    {
        $date = new DateTime('2017-01-01');
        $provider = new ExchangeRatesProvider();
        $provider->setDate($date);
        $this->assertSame($date, $provider->getDate());
    }

    public function testQuery(): void
    {
        $sub = $this->createMock(ExchangeRatesProvider::class);
        $sub->expects($this->any())
            ->method('query')
            ->will($this->returnValue($this->responseQuery))
        ;

        $date = '2016-01-07';
        $base = 'EUR';
        $targets = ['USD', 'JPY'];

        $query = $sub->query($date, $base, $targets);

        $this->assertEquals($date, $query['date']);
        $this->assertEquals($base, $query['base']);
        foreach ($targets as $target) {
            $this->assertArrayHasKey($target, $query['rates']);
        }
    }

    public function testGetHistorical(): void
    {
        $date = new DateTime('2016-01-07');
        $base = 'EUR';
        $targets = ['USD', 'JPY'];

        $response = $this->getResponseHistorical()->getHistorical($date, $base, $targets);
        $this->assertInstanceOf(DateTime::class, $response->getDate());
        $this->assertSame($base, $response->getBase());
        foreach (['USD', 'JPY'] as $rate) {
            $this->assertArrayHasKey($rate, $response->getRates());
        }
    }

    private function getResponseHistorical(): ExchangeRatesProvider
    {
        $sub = $this->createMock(ExchangeRatesProvider::class);
        $sub->expects($this->any())
            ->method('getHistorical')
            ->will($this->returnValue(
                new Result('EUR', new DateTime('2016-01-07'), ['USD' => 1.092459, 'JPY' => 128.7414])
            ))
        ;
        return $sub;
    }
}
