<?php

declare(strict_types=1);

namespace Tests\Service\CurrencyRate;

use CommissionTask\Service\CurrencyRate\Result;
use DateTime;
use PHPUnit\Framework\TestCase;

class ResultTest extends TestCase
{
    public function testResult(): void
    {
        $base = 'EUR';
        $date = new DateTime('2016-01-01');
        $rates = ['USD' => 1.092459, 'JPY' => 128.7414];

        $result = new Result($base, $date, $rates);

        $this->assertSame($base, $result->getBase());
        $this->assertSame($date, $result->getDate());
        $this->assertSame($rates, $result->getRates());
        $this->assertEquals($rates['USD'], $result->getRate('USD'));
        $this->assertEquals($rates['JPY'], $result->getRate('JPY'));
        $this->assertEquals(1, $result->getRate('EUR'));
        $this->assertNull($result->getRate('LTL'));
    }
}
