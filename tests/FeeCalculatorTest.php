<?php

declare(strict_types=1);

namespace Tests\Service;

use CommissionTask\Operation\Operation;
use CommissionTask\Service\FeeCalculator;
use PHPUnit\Framework\TestCase;

class FeeCalculatorTest extends TestCase
{
    /**
     * @dataProvider getOperationData
     */
    public function testFeeCalculator(array $operations, float $feeAmount): void
    {
        $feeCalculator = new FeeCalculator($operations);
        $feeCalculator->calculate();
        $operation = $operations[0];
        $this->assertEquals($feeAmount, $operation->getFeeAmount());
    }

    public function getOperationData(): iterable
    {
        $operation = new Operation('2016-01-01', 1, 'private', 'withdraw', 2000, 'EUR');
        $operation->setWeekAmount(1000);
        $operation->setWeekOperationNumber(1);
        yield 'testCalculate' => [[$operation], 3];
    }
}
