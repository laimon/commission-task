<?php

declare(strict_types=1);

namespace CommissionTask\Tests\Reader;

use CommissionTask\Reader\CsvReader;
use PHPUnit\Framework\TestCase;
use SplFileObject;

class CsvReaderTest extends TestCase
{
    public function testReaderCsvFile(): void
    {
        $file = new SplFileObject(__DIR__ . '/fixtures/data.csv');
        $csvReader = new CsvReader($file);

        $csvReader->setColumnHeaders(['date', 'user_id', 'user_type', 'type', 'amount', 'currency']);

        foreach ($csvReader as $row) {
            $this->assertNotNull($row['date']);
            $this->assertNotNull($row['user_id']);
            $this->assertNotNull($row['user_type']);
            $this->assertNotNull($row['type']);
            $this->assertNotNull($row['amount']);
            $this->assertNotNull($row['currency']);
        }
    }

    public function testCount(): void
    {
        $file = new SplFileObject(__DIR__ . '/fixtures/data.csv');
        $csvReader = new CsvReader($file);
        $csvReader->setColumnHeaders(['date', 'user_id', 'user_type', 'type', 'amount', 'currency']);

        $this->assertEquals(13, $csvReader->count());
    }
}
