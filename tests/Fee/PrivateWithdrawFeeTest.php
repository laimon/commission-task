<?php

declare(strict_types=1);

namespace Tests\Fee;

use CommissionTask\Fee\PrivateWithdrawFee;
use CommissionTask\Operation\Operation;
use PHPUnit\Framework\TestCase;

class PrivateWithdrawFeeTest extends TestCase
{
    private PrivateWithdrawFee $fee;

    public function setUp(): void
    {
        $this->fee = new PrivateWithdrawFee();
    }

    public function testCalculateCount(): void
    {
        $operation = new Operation('2016-01-06', 2, 'private', 'withdraw', 100, 'EUR');
        $operation->setWeekOperationNumber(4);
        $this->fee->setOperation($operation);
        $this->assertInstanceOf(Operation::class, $this->fee->operation);

        $calculation = $this->fee->calculate();
        $this->assertEquals(0.30, $calculation);
    }

    public function testCalculate(): void
    {
        $operation = new Operation('2016-01-06', 2, 'private', 'withdraw', 100, 'EUR');
        $operation->setWeekOperationNumber(1);
        $this->fee->setOperation($operation);
        $this->assertInstanceOf(Operation::class, $this->fee->operation);

        $calculation = $this->fee->calculate();
        $this->assertEquals(0.00, $calculation);
    }

    public function testCalculateOutmatchAmount(): void
    {
        $operation = new Operation('2016-01-06', 2, 'business', 'withdraw', 1200, 'EUR');
        $operation->setWeekAmount(200);
        $this->fee->setOperation($operation);
        $calculation = $this->fee->calculate();
        $this->assertEquals(0.6, $calculation);
    }
}
