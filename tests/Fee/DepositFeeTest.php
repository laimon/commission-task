<?php

declare(strict_types=1);

namespace Tests\Fee;

use CommissionTask\Fee\DepositFee;
use CommissionTask\Operation\Operation;
use PHPUnit\Framework\TestCase;

class DepositFeeTest extends TestCase
{
    public function testCalculate(): void
    {
        $operation = new Operation('2016-01-10', 2, 'business', 'withdraw', 10000, 'EUR');

        $depositFee = new DepositFee();
        $depositFee->setOperation($operation);

        $fee = $depositFee->calculate();

        $this->assertEquals(3.0, $fee);
    }
}
