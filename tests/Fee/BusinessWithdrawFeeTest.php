<?php

declare(strict_types=1);

namespace Tests\Fee;

use CommissionTask\Fee\BusinessWithdrawFee;
use CommissionTask\Operation\Operation;
use PHPUnit\Framework\TestCase;

class BusinessWithdrawFeeTest extends TestCase
{
    public function testCalculate(): void
    {
        $operation = new Operation('2016-01-06', 2, 'business', 'withdraw', 300, 'EUR');

        $businessWithdrawFee = new BusinessWithdrawFee();
        $businessWithdrawFee->setOperation($operation);

        $calculation = $businessWithdrawFee->calculate();

        $this->assertEquals(1.5, $calculation);
    }
}
