<?php

declare(strict_types=1);

namespace Tests;

use CommissionTask\CurrencyRateRegistry;
use CommissionTask\Service\CurrencyRate\Provider;
use CommissionTask\Service\CurrencyRate\Provider\DummyProvider;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class CurrencyRateRegistryTest extends TestCase
{
    private CurrencyRateRegistry $currencyRateRegistry;

    public function setUp(): void
    {
        $this->currencyRateRegistry = new CurrencyRateRegistry();
    }

    public function testCurrencyRateRegistry(): void
    {
        $key = 'dummyprovider';
        $provider = new DummyProvider();

        $this->currencyRateRegistry->set($key, $provider);
        $currencyRateProvider = $this->currencyRateRegistry->get($key);

        $this->assertInstanceOf(Provider::class, $currencyRateProvider);
    }

    public function testCurrencyRateRegistryWrongKey(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid key given');
        $wrongKey = 'errorprovider';
        $currencyRateProvider = $this->currencyRateRegistry->get($wrongKey);
    }
}
