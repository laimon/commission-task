<?php

declare(strict_types=1);

namespace Tests\Operation;

use CommissionTask\Operation\Operation;
use PHPUnit\Framework\TestCase;

class OperationTest extends TestCase
{
    public function testConstructOperation(): Operation
    {
        $date = "2014-12-31";
        $userId = 1;
        $userType = "private";
        $type = "withdraw";
        $amount = 1200.00;
        $currency = "EUR";

        $operation = new Operation($date, $userId, $userType, $type, $amount, $currency);

        $this->assertInstanceOf(Operation::class, $operation);
        $this->assertInstanceOf(\DateTime::class, $operation->getDate());
        $this->assertSame($date, $operation->getDate()->format('Y-m-d'));
        $this->assertSame($userId, $operation->getUserId());
        $this->assertSame($userType, $operation->getUserType());
        $this->assertSame($type, $operation->getType());
        $this->assertSame($amount, $operation->getAmount());
        $this->assertSame($currency, $operation->getCurrency());

        return $operation;
    }

    /**
     * @depends testConstructOperation
     */
    public function testSetDate(Operation $operation): void
    {
        $newDate = '2016-01-01';
        $operation->setDate($newDate);

        $this->assertInstanceOf(\DateTime::class, $operation->getDate());
        $this->assertSame($newDate, $operation->getDate()->format('Y-m-d'));
    }

    /**
     * @depends testConstructOperation
     */
    public function testGetDate(Operation $operation): void
    {
        $this->assertInstanceOf(\DateTime::class, $operation->getDate());
    }

    /**
     * @depends testConstructOperation
     */
    public function testSetUserId(Operation $operation): void
    {
        $newUserId = 2;
        $operation->setUserId($newUserId);
        $this->assertSame($newUserId, $operation->getUserId());
    }

    /**
     * @depends testConstructOperation
     */
    public function testSetUserType(Operation $operation): void
    {
        $newUserType = "business";
        $operation->setUserType($newUserType);
        $this->assertSame($newUserType, $operation->getUserType());
    }

    /**
     * @depends testConstructOperation
     */
    public function testSetType(Operation $operation): void
    {
        $newType = "deposit";
        $operation->setType($newType);
        $this->assertSame($newType, $operation->getType());
    }

    /**
     * @depends testConstructOperation
     */
    public function testSetAmount(Operation $operation): void
    {
        $newAmount = 3000000.00;
        $operation->setAmount($newAmount);
        $this->assertSame($newAmount, $operation->getAmount());
    }

    /**
     * @depends testConstructOperation
     */
    public function testSetCurrency(Operation $operation): void
    {
        $newCurrency = "USD";
        $operation->setCurrency($newCurrency);
        $this->assertSame($newCurrency, $operation->getCurrency());
    }

    /**
     * @depends testConstructOperation
     */
    public function testSetWeek(Operation $operation): void
    {
        $newWeek = 20;
        $operation->setWeek($newWeek);
        $this->assertSame($newWeek, $operation->getWeek());
    }

    /**
     * @depends testConstructOperation
     */
    public function testSetWeekAmount(Operation $operation): void
    {
        $newWeekAmount = 1200.0;
        $operation->setWeekAmount($newWeekAmount);
        $this->assertSame($newWeekAmount, $operation->getWeekAmount());
    }

    /**
     * @depends testConstructOperation
     */
    public function testSetRate(Operation $operation): void
    {
        $newRate = 1.02;
        $operation->setRate($newRate);
        $this->assertSame($newRate, $operation->getRate());
    }

    public function testSetOldWeekNewYear(): void
    {
        $operation = new Operation('2021-01-01', 1, 'private', 'withdraw', 1000, 'JPY');

        $this->assertEquals(2020, $operation->getWeekYear());
    }

    public function testSetNewWeekOldYear(): void
    {
        $operation = new Operation('2019-12-31', 1, 'private', 'withdraw', 1000, 'JPY');

        $this->assertEquals(2020, $operation->getWeekYear());
    }

    public function testSetSameWeekOldYear(): void
    {
        $operation = new Operation('2017-12-31', 1, 'private', 'withdraw', 1000, 'JPY');

        $this->assertEquals(2017, $operation->getWeekYear());
    }

    public function testSameWeekNewYear(): void
    {
        $operation = new Operation('2018-01-01', 1, 'private', 'withdraw', 1000, 'JPY');

        $this->assertEquals(2018, $operation->getWeekYear());
    }

    public function testToStringFeeAmount(): void
    {
        $operation = new Operation('2018-01-01', 1, 'private', 'withdraw', 2000, 'EUR');
        $operation->setFeeAmount(3);
        $this->assertSame('3.00', $operation->toStringFeeAmount());
    }
}
